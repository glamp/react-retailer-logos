import React from 'react';
import ReactDOM from 'react-dom';
import RetailerLogo, { brands } from './src/index';

const Demo = () => (
  <div>
  {
    brands.map(brand => (
      <center style={{ width: 150, display: 'inline-block', margin: 10, padding: 10, border: '1px solid coral'  }}>
        <p>{brand}</p>
        <RetailerLogo brand={brand} height={64}  />
      </center>
    ))
  }
  </div>
);


ReactDOM.render(<Demo />, document.getElementById('demo'));


#!/bin/bash
set -e
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"


BASE64_PNG=$(cat ${DIR}/../src/brands.png | base64)
echo "module.exports = 'data:image/png;base64,${BASE64_PNG}';" > ${DIR}/../src/brands.js

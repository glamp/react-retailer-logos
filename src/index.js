import React from 'react';
import positionLookup from './position-lookup';
import getBrandsImage from './get-brands-image';


const Sprite = ({ image, x, y, width, height, spriteWidth, spriteHeight, style }) => {
  const bgX = (width / spriteWidth) * 896;
  const bgY = (height / spriteHeight) * 3584;

  const xMargin = 0;
  const yMargin = 0;
  const xPos = (width * x) + (xMargin * x);
  const yPos = (height * y) + (yMargin * y);

  const brands = getBrandsImage(true);

  return (
    <div style={{
      backgroundImage: `url(${brands})`,
      backgroundPosition:  `-${xPos}px -${yPos}px`,
      backgroundRepeat: 'no-repeat',
      backgroundSize: `${bgX}px ${bgY}px`,
      display: 'inline-block',
      width,
      height,
      ...style
    }} />
  )
}


exports.brands = positionLookup.map(x => x.domain);

export default ({ sprite, brand, height, width, style }) => {
  style = style || {};

  const SPRITE_WIDTH = 128;
  const SPRITE_HEIGHT = 128;

  const N_ROWS = 28;

  let x, y;
  for (var i = 0; i < positionLookup.length; i++) {
    if (positionLookup[i].domain===brand) {
      x = positionLookup[i].column - 1;
      y = positionLookup[i].row - 1;
    }
  }

  const pos = { x, y };

  return (
    <Sprite
      x={pos.x}
      y={pos.y}
      width={width || 64}
      height={height || 64}
      spriteWidth={SPRITE_WIDTH}
      spriteHeight={SPRITE_HEIGHT}
      style={style}
    />
  );

}
